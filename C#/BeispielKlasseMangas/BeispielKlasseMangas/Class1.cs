﻿using System;

namespace MangaLibrary
{
    //Klasse
    public class Manga
    {
        //Attribute
        public string Title;
        public string Author;
        public string MainCharacter;
        public string Illustrator;
        public string Publisher;
        public int    Volume;
        public string Genre; 

        //Konstruktor
        public Manga(string title, string author,string mainCharacter, string illustrator, string publisher, int volume, string genre)
        {
            Title = title;
            Author = author;
            MainCharacter = mainCharacter;
            Illustrator = illustrator;
            Publisher = publisher;
            Volume = volume;
            Genre = genre;
        }

        //Methoden (Die Methode "ToString" überschreibt die Methode aus der Basisklasse "Object" und gibt alle Attribute in einer lesbaren Form zurück.)
        public override string ToString()
        {
            return $"Title: {Title}, Author: {Author}, MainCharacter: {MainCharacter}, Illustrator: {Illustrator}, Publisher: {Publisher}, Volume: {Volume}, Genre: {Genre}, ";
        }
    }
}
