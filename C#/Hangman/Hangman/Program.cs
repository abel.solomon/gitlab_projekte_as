﻿


using System;

namespace HangmanGame
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] words = { "programmieren", "computer", "konsolenspiel", "entwicklung", "software" };
            Random rand = new Random();
            string wordToGuess = words[rand.Next(words.Length)];
            char[] guessedLetters = new char[wordToGuess.Length];

            for (int i = 0; i < guessedLetters.Length; i++)
            {
                guessedLetters[i] = '_';
            }

            int attemptsLeft = 6;
            bool wordGuessed = false;

            while (attemptsLeft > 0 && !wordGuessed)
            {
                Console.WriteLine("Gesuchtes Wort: " + String.Join(" ", guessedLetters));
                Console.WriteLine("Versuche übrig: " + attemptsLeft);
                Console.Write("Gib einen Buchstaben ein: ");
                char input = Console.ReadLine()[0];

                if (wordToGuess.Contains(input))
                {
                    for (int i = 0; i < wordToGuess.Length; i++)
                    {
                        if (wordToGuess[i] == input)
                        {
                            guessedLetters[i] = input;
                        }
                    }
                }
                else
                {
                    attemptsLeft--;
                }

                if (String.Join("", guessedLetters) == wordToGuess)
                {
                    wordGuessed = true;
                }
            }

            if (wordGuessed)
            {
                Console.WriteLine("Herzlichen Glückwunsch! Du hast das Wort erraten: " + wordToGuess);
            }
            else
            {
                Console.WriteLine("Leider hast du das Wort nicht erraten. Das Wort war: " + wordToGuess);
            }

            Console.ReadKey();
        }
    }
}


