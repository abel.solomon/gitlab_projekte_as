#include <stdio.h>


int main()
{
    //Deklarationen
    float celsius;
    float fahrenheit;
    int nochmals;
    

    //Titel
    printf("Celisus zu Fahrenheit\n");
    printf("----------------------------------------------------\n\n");

    //Wiederhol-Schleife
    do
    {

        //Eingabe
        printf("Bitte geben Sie die Temperatur in Celsius ein: ");
        scanf("%f", &celsius);

        //Verarbeitung
        fahrenheit = celsius*(9.0/5) + 32;

        //Ausgabe
        printf("%.1f Grad Celsius = %.1f Grad Fahrenheit\n\n", celsius, fahrenheit);

        //Wiederhohlen?
        printf("Noche eine Umrechnung ? (nein=0 / ja=1): ");
        scanf("%d", &nochmals);
        printf("----------------------------------------------------\n\n");

    }
    while(nochmals> 0);

    //Fertig
    printf("Tschuess...\n");



    return 0;
}