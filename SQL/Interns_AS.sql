-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: localhost    Database: interns
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `besizt`
--

DROP TABLE IF EXISTS `besizt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `besizt` (
  `Skills_Sklillid` int NOT NULL,
  `Talent_Talentid` int NOT NULL,
  PRIMARY KEY (`Skills_Sklillid`,`Talent_Talentid`),
  KEY `fk_Skills_has_Talent_Talent1_idx` (`Talent_Talentid`),
  KEY `fk_Skills_has_Talent_Skills_idx` (`Skills_Sklillid`),
  CONSTRAINT `fk_Skills_has_Talent_Skills` FOREIGN KEY (`Skills_Sklillid`) REFERENCES `skills` (`Sklillid`),
  CONSTRAINT `fk_Skills_has_Talent_Talent1` FOREIGN KEY (`Talent_Talentid`) REFERENCES `talent` (`Talentid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `besizt`
--

LOCK TABLES `besizt` WRITE;
/*!40000 ALTER TABLE `besizt` DISABLE KEYS */;
/*!40000 ALTER TABLE `besizt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informatikchallenge`
--

DROP TABLE IF EXISTS `informatikchallenge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `informatikchallenge` (
  `1001` int DEFAULT NULL,
  `10115` int DEFAULT NULL,
  `ABC GmbH` text,
  `Berlin` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informatikchallenge`
--

LOCK TABLES `informatikchallenge` WRITE;
/*!40000 ALTER TABLE `informatikchallenge` DISABLE KEYS */;
INSERT INTO `informatikchallenge` VALUES (1001,10115,'ABC GmbH','Berlin'),(2002,80331,'XYZ AG','MÃ¼nchen'),(3003,20095,'DEF KG','Hamburg'),(4004,40213,'GHI GmbH & Co. KG','DÃ¼sseldorf'),(5005,50667,'JKL GmbH','KÃ¶ln');
/*!40000 ALTER TABLE `informatikchallenge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `praktikumstelle`
--

DROP TABLE IF EXISTS `praktikumstelle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `praktikumstelle` (
  `Praktikumstelleid` int NOT NULL,
  `Praktikum Name` varchar(45) DEFAULT NULL,
  `Adresse` varchar(45) DEFAULT NULL,
  `Anzahl freie Stellen` varchar(45) DEFAULT NULL,
  `PLZ` int DEFAULT NULL,
  `Erforderliche skills` varchar(45) DEFAULT NULL,
  `Praktikum Lohn` int DEFAULT NULL,
  `Praktikumstellecol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Praktikumstelleid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `praktikumstelle`
--

LOCK TABLES `praktikumstelle` WRITE;
/*!40000 ALTER TABLE `praktikumstelle` DISABLE KEYS */;
/*!40000 ALTER TABLE `praktikumstelle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skills`
--

DROP TABLE IF EXISTS `skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `skills` (
  `Sklillid` int NOT NULL,
  `Skill_Name` varchar(45) DEFAULT NULL,
  `Anzahl_Skills` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Sklillid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skills`
--

LOCK TABLES `skills` WRITE;
/*!40000 ALTER TABLE `skills` DISABLE KEYS */;
INSERT INTO `skills` VALUES (1,'SQL','15');
/*!40000 ALTER TABLE `skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `talent`
--

DROP TABLE IF EXISTS `talent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `talent` (
  `Talentid` int NOT NULL,
  `Talent_Alter` int DEFAULT NULL,
  `Talentname` varchar(45) DEFAULT NULL,
  `Adresse` varchar(45) DEFAULT NULL,
  `PLZ` int DEFAULT NULL,
  `Unternehmen_Unternehmenid` int NOT NULL,
  `Praktikumstelle_Praktikumstelleid` int NOT NULL,
  PRIMARY KEY (`Talentid`,`Unternehmen_Unternehmenid`,`Praktikumstelle_Praktikumstelleid`),
  KEY `fk_Talent_Unternehmen1_idx` (`Unternehmen_Unternehmenid`),
  KEY `fk_Talent_Praktikumstelle1_idx` (`Praktikumstelle_Praktikumstelleid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `talent`
--

LOCK TABLES `talent` WRITE;
/*!40000 ALTER TABLE `talent` DISABLE KEYS */;
INSERT INTO `talent` VALUES (1,17,'Solomon','Flurhofstrasse 155',9000,1,1);
/*!40000 ALTER TABLE `talent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unternehmen`
--

DROP TABLE IF EXISTS `unternehmen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `unternehmen` (
  `Unternehmenid` int NOT NULL,
  `PLZ` int DEFAULT NULL,
  `Unternehmungs Name` varchar(45) DEFAULT NULL,
  `Ort` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Unternehmenid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unternehmen`
--

LOCK TABLES `unternehmen` WRITE;
/*!40000 ALTER TABLE `unternehmen` DISABLE KEYS */;
/*!40000 ALTER TABLE `unternehmen` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-06-13 20:10:46
